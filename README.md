# python-rrdtool安装

1. **yum安装rrdtool**

```sh
yum -y install rrdtool rrdtool-devel
```

```sh
yum install gcc cmake make python-devel cairo-devel libxml2 libxml2-devel pango-devel pango libpng-devel freetype freetype-devel libart_lgpl-devel -y

```

2. **使用pip安装python-rrdtool模块**

```sh
pip install rrdtool
```

`pip`未安装则运行下面这段命令

```sh
curl https://bootstrap.pypa.io/get-pip.py -o get-pip.py   # 下载安装脚本
sudo python get-pip.py # 运行安装脚本
sudo python get-pip.py	# 运行安装脚本。
pip install rrdtool
```

3. **安装完成后测试**

```python
python
>>> import rrdtool
```
4. **rrdtool常用命令**
- 查看rrd文件内容有利于观察数据的结构、更新等情况，rrdtool提供几个常用命令：
- info查看rrd文件的结构信息，如`rrdtool info Flow.rrd`；
- first查看rrd文件第一个数据的更新时间，如`rrdtool first Flow.rrd`；
- last查看rrd文件最近一次更新的时间，如`rrdtool last Flow.rrd`；
- fetch根据指定时间、CF查询rrd文件，如`rrdtool fetch Flow.rrd AVERAGE`。
