#!/bin/bash
source /etc/profile
source ~/.bash_profile
TIMESTAMP=`date +%Y-%m-%d\ %I:%M:%S\ %p\ %A`
if  [ ! -e '/usr/local/sbin/fping' ]; then
    echo "正在安装fping。。。"
	wget http://fping.org/dist/fping-4.2.tar.gz
	tar -zxvf fping-4.2.tar.gz
	cd fping-4.2
	./configure
	make && make install
	rm -rf fping-4.2.tar.gz fping-4.2
fi
fping -f pinglist >>isalive
while read line
do
	isflag=`echo $line | awk -F '[ ]+' '{print $3}'`
	if [ "$isflag" = "unreachable" ];then
		host=`echo $line | awk -F '[ ]+' '{print $1}'`
		echo "fping,host=${host} latency=0,package=\"100%\"">>insertsql
	fi
done < isalive
fping -c1 -b1024 -f pinglist >>pinglog
printf "%-19s%-10s%--12s\n" "服务器IP" "延迟" "丢包率"
while read line
do
	echo $line | awk -F '[ ]+' '{print $1"	"$6$7"	"$10}'
	host=`echo $line | awk -F '[ ]+' '{print $1}'`
	latency=`echo $line | awk -F '[ ]+' '{print $6}'`
	package=`echo $line | awk -F '[ ]+' '{print $10}'`
	echo -e "fping,host=${host} latency=${latency},package=\"${package}\"">>insertsql
done < pinglog
curl -i -XPOST 'http://localhost:8086/write?db=mydb' --data-binary @insertsql
rm -f pinglog insertsql isalive
